package com.example.test;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;


public class AddKanbanCardActivity extends AppCompatActivity {

    public int taskIndex;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_add_kanban_card);
        Intent intent = getIntent();
        taskIndex = intent.getIntExtra("taskID", 0);


    }

    public void createCardButtonClicked(View view) {
        TextView cardName = findViewById(R.id.cardNameText);
        TextView cardDescription = findViewById(R.id.cardDescription);
        String cardNameText = cardName.getText().toString();
        String cardDescriptionText = cardDescription.getText().toString();
        KanbanCard newCard = new KanbanCard(cardNameText, cardDescriptionText);
        MainActivity.tasks.get(taskIndex).cards.add(newCard);
        Intent data = new Intent();
        data.putExtra("cardName", cardNameText);
        data.putExtra("cardDescription", cardDescriptionText);

        setResult(RESULT_OK, data);
        finish();

    }
}