package com.example.test;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

public class TransitActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_transit);
        Intent methodIntent = getIntent();


        Intent mainIntent = new Intent(this, MainActivity.class);
        Intent resultIntent = new Intent(this, CourseWorkActivity.class);
        startActivity(mainIntent);
        Toast.makeText(this, "Main", Toast.LENGTH_LONG).show();
        openData();
        startActivity(resultIntent);
        Toast.makeText(this, "Result", Toast.LENGTH_LONG).show();
        finish();
    }

    public void openData() {

        MainActivity.tasks = JSONHelper.importFromJSON(this);

        if (MainActivity.tasks != null) {
            Toast.makeText(this, "Т Данные восстановлены", Toast.LENGTH_LONG).show();
            //String listSize = String.valueOf(tasks.size());
            //Toast.makeText(this, listSize, Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "Т Не удалось открыть данные", Toast.LENGTH_LONG).show();
        }
    }

}