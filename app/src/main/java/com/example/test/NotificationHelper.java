package com.example.test;
import android.annotation.TargetApi;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.os.Build;

import androidx.core.app.NotificationCompat;
import androidx.core.app.TaskStackBuilder;


public class NotificationHelper extends ContextWrapper {
    public static final String channelID = "channelID";
    public static final String channelName = "Channel Name";
    private static final int notifyId = 101;

    private NotificationManager mManager;

    public NotificationHelper(Context base) {
        super(base);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createChannel();
        }
    }

    @TargetApi(Build.VERSION_CODES.O)
    private void createChannel() {
        NotificationChannel channel = new NotificationChannel(channelID, channelName, NotificationManager.IMPORTANCE_HIGH);

        getManager().createNotificationChannel(channel);
    }

    public NotificationManager getManager() {
        if (mManager == null) {
            mManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        }

        return mManager;
    }

    public NotificationCompat.Builder getChannelNotification() {
        Intent mainIntent = new Intent(getApplicationContext(), MainActivity.class);
        //mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        //PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, resultIntent, PendingIntent.FLAG_MUTABLE);
        Intent transitIntent = new Intent(getApplicationContext(), TransitActivity.class);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(getApplicationContext());
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(mainIntent);

        Intent resultIntent = new Intent(getApplicationContext(), CourseWorkActivity.class);
        //resultIntent.putExtra(TASK_NAME, "notification test");
        //resultIntent.putExtra(TASK_ID, taskIndex);
        stackBuilder.addNextIntent(resultIntent);

        //Intent[] intents = new Intent[]{mainIntent, resultIntent};

        PendingIntent resultPendingIntent = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
            resultPendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, transitIntent, PendingIntent.FLAG_MUTABLE);
        }
        else {
            resultPendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, transitIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        }


        NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        //button = findViewById(R.id.button);0
        return new NotificationCompat.Builder(getApplicationContext(), channelID)
                .setAutoCancel(false)
                .setSmallIcon(R.drawable.ic_calendar)
                .setContentIntent(resultPendingIntent)
                .setContentTitle("Title")
                .setContentText("text");
    }

    public void restartNotify(int taskIndex) {
        Intent mainIntent = new Intent(getApplicationContext(), MainActivity.class);
        //mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        //PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, resultIntent, PendingIntent.FLAG_MUTABLE);
        Intent transitIntent = new Intent(getApplicationContext(), TransitActivity.class);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(getApplicationContext());
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(mainIntent);

        Intent resultIntent = new Intent(getApplicationContext(), CourseWorkActivity.class);
        //resultIntent.putExtra(TASK_NAME, "notification test");
        //resultIntent.putExtra(TASK_ID, taskIndex);
        stackBuilder.addNextIntent(resultIntent);

        //Intent[] intents = new Intent[]{mainIntent, resultIntent};

        PendingIntent resultPendingIntent = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
            resultPendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, transitIntent, PendingIntent.FLAG_MUTABLE);
        }
        else {
            resultPendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, transitIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        }


        NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        //button = findViewById(R.id.button);0
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(getApplicationContext(), channelID)
                        .setAutoCancel(false)
                        .setSmallIcon(R.drawable.ic_calendar)
                        .setWhen(System.currentTimeMillis())
                        .setContentIntent(resultPendingIntent)
                        .setContentTitle("Title")
                        .setContentText("text");
        //.setPriority(PRIORITY_HIGH);

        createChannelIfNeeded(notificationManager);
        notificationManager.notify(notifyId, notificationBuilder.build());

    }

    public static void createChannelIfNeeded(NotificationManager manager) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(channelID, channelID, NotificationManager.IMPORTANCE_DEFAULT);
            manager.createNotificationChannel(notificationChannel);
        }
    }
}