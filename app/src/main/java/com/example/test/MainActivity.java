package com.example.test;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.app.TaskStackBuilder;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {


    public SharedPreferences prefs;
    public SharedPreferences preSettingsPrefs;

    public int taskAmount = 0;
    public static List<Task> tasks;
    public AlarmManager alarmManager;


    final String FIRST_RUN = "firstRun";
    //final String TEST_TEXTVIEW_ID = "testTextViewId";
    final String PRE_SETTINGS = "preSettings";
    final String INPUT_DATA = "inputData";

    private static String CHANNEL_ID = "Test channel";
    private static final int NOTIFY_ID = 101;
    static final String RESPONSE_MESSAGE = "responseMessage";
    static final String IS_CREATED = "taskCreated";
    static final String TASK_NAME = "taskName";
    static final String TASK_AMOUNT = "taskAmount";
    static final String SELECTED_METHOD = "selectedMethod";
    static final String TASK_ID = "taskID";

    static final String FILE_NAME = "content.txt";


    final int btnHeightPixels = 54;


    ActivityResultLauncher<Intent> mStartForResult = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {

                    if (result.getResultCode() == Activity.RESULT_OK) {
                        Intent intent = result.getData();
                        String taskName = intent.getStringExtra(TASK_NAME);
                        String selectedMethod = intent.getStringExtra(SELECTED_METHOD);
                        addNewTask(taskName, selectedMethod, 0);


                    } else {

                    }
                }
            });


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();
        prefs = getSharedPreferences(FIRST_RUN, MODE_PRIVATE);
        preSettingsPrefs = getSharedPreferences(PRE_SETTINGS, MODE_PRIVATE);
        preSettingsPrefs = getSharedPreferences(INPUT_DATA, MODE_PRIVATE);
        preSettingsPrefs = getSharedPreferences(IS_CREATED, MODE_PRIVATE);
        preSettingsPrefs = getSharedPreferences(TASK_NAME, MODE_PRIVATE);

        tasks = new ArrayList<>();


        if (prefs.getBoolean(FIRST_RUN, true)) {
            Intent intent = new Intent(this, AddNewTaskActivity.class);
            intent.putExtra(TASK_NAME, "123");
            //intent.putExtra(RESPONSE_MESSAGE, testText);
            mStartForResult.launch(intent);

            prefs.edit().putBoolean(FIRST_RUN, false).commit();
        } else {
            openData();
            Toast.makeText(this, "Opened", Toast.LENGTH_LONG).show();
            restoreData();
        }

    }

    @Override
    protected void onResume(){
        super.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
        LinearLayout taskLayout = findViewById(R.id.taskLayout);
        saveData();
        //restartNotify();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }


    @SuppressLint("UseCompatLoadingForDrawables")
    public void addNewTask(String taskName, String method, int taskID) {

        LinearLayout taskLayout = findViewById(R.id.taskLayout);
        Button addedTaskBtn = new Button(this);
        addedTaskBtn.setText(taskName);
        //taskAmount++;
        Task addedTask = new Task();
        addedTask.id = taskID;
        addedTask.name = taskName;
        addedTask.method = method;
        //tasks.add(addedTask);
        //saveData();


        addedTaskBtn.setBackground(getDrawable(R.drawable.whitebuttonbackground));
        float factor = addedTaskBtn.getResources().getDisplayMetrics().density;


        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                (int) (btnHeightPixels * factor));

        //поменять на немагические значения
        lp.setMargins(0, 0, 0, (int) (7 * factor));

        addedTaskBtn.setLayoutParams(lp);
        addedTaskBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView debugTextView = findViewById(R.id.debugTextView);
                debugTextView.setText("worked");
                Context context = getApplicationContext();
                switch (method) {
                    case "Курсовая работа":
                        Intent intent = new Intent(context, CourseWorkActivity.class);
                        //intent.putExtra(TASK_NAME, taskName);
                        intent.putExtra(TASK_ID, taskID);
                        startActivity(intent);
                        break;
                    default:
                        debugTextView.setText("не курсовая");
                }
                //Intent intent = new Intent(context, CourseWorkActivity.class);

            }


        });

        taskLayout.addView(addedTaskBtn, 0);
        //taskLayout.addView(addNewTaskBtn);


    }

    public void addNewTaskButtonClicked(View view) {
        Intent intent = new Intent(this, AddNewTaskActivity.class);
        intent.putExtra(TASK_NAME, "1234");
        mStartForResult.launch(intent);
    }

    public void saveButtonClicked(View view) {
        startAlarm(0);
        //restartNotify(0);
        //Toast.makeText(this, "Уведомление", Toast.LENGTH_LONG).show();
    }


    public void saveData() {
        boolean result = JSONHelper.exportToJSON(this, tasks);
        if (result) {
            Toast.makeText(this, "Данные сохранены", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "Не удалось сохранить данные", Toast.LENGTH_LONG).show();
        }
    }

    public void openData() {

        tasks = JSONHelper.importFromJSON(this);

        if (tasks != null) {
            Toast.makeText(this, "Данные восстановлены", Toast.LENGTH_LONG).show();
            //String listSize = String.valueOf(tasks.size());
            //Toast.makeText(this, listSize, Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "Не удалось открыть данные", Toast.LENGTH_LONG).show();
        }
    }

    public void restoreData() {
        for (int idx = 0; idx < tasks.size(); idx++) {
            addNewTask(tasks.get(idx).name, tasks.get(idx).method, tasks.get(idx).id);
            //Toast.makeText(this, String.valueOf(idx), Toast.LENGTH_LONG).show();
        }
    }

    public void restartNotify(int taskIndex) {
        Intent mainIntent = new Intent(getApplicationContext(), MainActivity.class);
        //mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        //PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, resultIntent, PendingIntent.FLAG_MUTABLE);
        Intent transitIntent = new Intent(getApplicationContext(), TransitActivity.class);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(getApplicationContext());
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(mainIntent);

        Intent resultIntent = new Intent(getApplicationContext(), CourseWorkActivity.class);
        //resultIntent.putExtra(TASK_NAME, "notification test");
        resultIntent.putExtra(TASK_ID, taskIndex);
        stackBuilder.addNextIntent(resultIntent);

        //Intent[] intents = new Intent[]{mainIntent, resultIntent};

        PendingIntent resultPendingIntent = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
            resultPendingIntent = PendingIntent.getActivity(this, 0, transitIntent, PendingIntent.FLAG_MUTABLE);
        }
        else {
            resultPendingIntent = PendingIntent.getActivity(this, 0, transitIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        }


        NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        //button = findViewById(R.id.button);0
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(getApplicationContext(), CHANNEL_ID)
                        .setAutoCancel(false)
                        .setSmallIcon(R.drawable.ic_calendar)
                        .setWhen(System.currentTimeMillis())
                        .setContentIntent(resultPendingIntent)
                        .setContentTitle("Title")
                        .setContentText("text");
                        //.setPriority(PRIORITY_HIGH);

        createChannelIfNeeded(notificationManager);
        notificationManager.notify(NOTIFY_ID, notificationBuilder.build());

    }

    public static void createChannelIfNeeded(NotificationManager manager) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID, CHANNEL_ID, NotificationManager.IMPORTANCE_DEFAULT);
            manager.createNotificationChannel(notificationChannel);
        }
    }

    public void startAlarm(int taskIndex) {
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(this, AlertReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, taskIndex, intent, PendingIntent.FLAG_MUTABLE);


        alarmManager.setExact(AlarmManager.RTC_WAKEUP,  tasks.get(taskIndex).startDate.getTime(), pendingIntent);
    }

    public void cancelAlarm(int taskIndex) {

    }


}

