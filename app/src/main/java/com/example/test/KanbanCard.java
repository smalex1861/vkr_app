package com.example.test;

public class KanbanCard {

    public long id;

    public String name;

    public String description;

    //public String deadlineDate;

    //public String time;

    //public CheckList subtasks;
    public KanbanCard(String name, String description) {
        this.name = name;
        this.description = description;
    }

}
