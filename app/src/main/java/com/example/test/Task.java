package com.example.test;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class Task {


    public int id;

    public String name;

    public String method;

    public Date startDate;

    public Date endDate;

    public List<KanbanCard> cards;

    public String data;

    //public String notificationTime;

    //сделать даты
    public Task() {
        cards = new ArrayList<>();
    }

//    public long getId() { return id; }
//
//    public void setId(long id) { this.id = id; }
//
//    public String getName() { return name; }
//
//    public void setName(String name) { this.name = name; }
//
//    public String getData() { return data; }
//
//    public void setData(String data) { this.data = data; }
}
