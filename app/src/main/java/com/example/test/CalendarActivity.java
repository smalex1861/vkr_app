package com.example.test;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.widget.CalendarView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class CalendarActivity extends AppCompatActivity {

    Calendar calendar = Calendar.getInstance();
    CalendarView calendarView;
    TextView calendarText;
    String enteredType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String languageToLoad  = "ru"; // your language
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());

        setContentView(R.layout.activity_calendar);
        getSupportActionBar().hide();
        Intent intent = getIntent();
        TextView header = findViewById(R.id.chooseDateHeader);
        enteredType = intent.getStringExtra("type");
        header.setText(enteredType);

        calendarView = findViewById(R.id.calendarView);

        calendarText = findViewById(R.id.calendarText);
        Date currentDate = new Date();

        calendarView.setMinDate(currentDate.getTime());
        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView calendarView, int year,
                                            int month, int dayOfMonth) {

                calendar.set(year, month, dayOfMonth);
            }
        });




        }

    public void applyDateClicked(View view) {

        DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM);
        Intent date = new Intent();

        calendarText.setText(dateFormat.format(calendar.getTime()));

        if(enteredType.equals("Выбор стартовой даты")) {
            AddNewTaskActivity.startTaskCalendar.setTimeInMillis(calendarView.getDate());
            date.putExtra("startDate", dateFormat.format(calendar.getTime()));
        }
        else if(enteredType.equals("Выбор конечной даты")) {
            AddNewTaskActivity.endTaskCalendar.setTimeInMillis(calendarView.getDate());
            date.putExtra("endDate", dateFormat.format(calendar.getTime()));
        }
        Toast.makeText(this, dateFormat.format(calendar.getTime()), Toast.LENGTH_LONG).show();
        setResult(RESULT_OK, date);
        finish();
    }

    public void applyDate(){
        AddNewTaskActivity.startTaskCalendar.setTimeInMillis(calendarView.getDate());
    }

}
