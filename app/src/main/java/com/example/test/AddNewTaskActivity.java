package com.example.test;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;


public class AddNewTaskActivity extends AppCompatActivity  {

    final public String[] arrayMethods = new String [] {
            "Курсовая работа",
            "Подготовка к аудиторным занятиям",
            "Домашнее задание",
            "Работа с информацией",
            "Проектная работа",
            "Текстовый редактор",
            "Канбан",
            "Чек-лист",
            "Матрица Эйзенхауэра"
    };
    static int taskId = 0;
    int DIALOG_TIME = 1;
    Task task = new Task();
    static Calendar startTaskCalendar = Calendar.getInstance();
    static Calendar endTaskCalendar = Calendar.getInstance();
    boolean isStartDateAdded = false;
    boolean isEndDateAdded = false;
    boolean isOneDay = true;
    Button addStartTimeButton;
    Button addEndTimeButton;
    Button startDateButton;
    Button endDateButton;


    ActivityResultLauncher<Intent> mStartForResult = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {

                    if (result.getResultCode() == Activity.RESULT_OK) {


                        Intent intent = result.getData();
                        String startChosenDate = intent.getStringExtra("startDate");
                        String endChosenDate = intent.getStringExtra("endDate");
                        setStartDate(startChosenDate);
                        setEndDate(endChosenDate);
                        //String selectedMethod = intent.getStringExtra(SELECTED_METHOD);
                        //addNewTask(taskName, selectedMethod, 0);


                    } else {

                    }
                }
            });


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_add_new_task);


        addStartTimeButton = findViewById(R.id.addStartTimeButton);
        addEndTimeButton = findViewById(R.id.addEndTimeButton);

        endDateButton = findViewById(R.id.endDateButton);
        startDateButton = findViewById(R.id.startDateButton);

        TextView endTimeTextView = findViewById(R.id.endTimeTextView);
        TextView endDateTextView = findViewById(R.id.endDateTextView);


        Spinner methodSpinner = findViewById(R.id.methodSpinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, arrayMethods);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        methodSpinner.setAdapter(adapter);

        methodSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View selectedItem, int selectedItemPosition, long selectedId) {
                if(arrayMethods[selectedItemPosition].equals("Курсовая работа") || arrayMethods[selectedItemPosition].equals("Проектная работа")) {

                }
                else {

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        @SuppressLint("UseSwitchCompatOrMaterialCode") Switch oneDaySwitch = findViewById(R.id.oneDaySwitch);
        oneDaySwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked){
                endDateButton.setVisibility(View.INVISIBLE);
                endDateTextView.setVisibility(View.INVISIBLE);
                endTimeTextView.setVisibility(View.INVISIBLE);
                addEndTimeButton.setVisibility(View.INVISIBLE);
                isOneDay = true;
                //task.endDate = task.startDate;
                }
                else {
                    endDateButton.setVisibility(View.VISIBLE);
                    endDateTextView.setVisibility(View.VISIBLE);
                    endTimeTextView.setVisibility(View.VISIBLE);
                    addEndTimeButton.setVisibility(View.VISIBLE);
                    isOneDay = false;
                }
            }
        });

    }

    public void createTaskClick(View view) {
        Spinner methodSpinner = findViewById(R.id.methodSpinner);
        String selectedMethod = methodSpinner.getSelectedItem().toString();
        EditText editTaskName = findViewById(R.id.editTaskName);
        if(MainActivity.tasks.size() > 0) {
            taskId = MainActivity.tasks.size();
        }
        String taskName = editTaskName.getText().toString();

        task.id = taskId;
        task.name = taskName;
        task.method = selectedMethod;
        task.startDate = startTaskCalendar.getTime();
        if(isOneDay){
            task.endDate = task.startDate;
        }
        else {
            task.endDate = endTaskCalendar.getTime();
        }



        Intent data = new Intent();
        data.putExtra(MainActivity.IS_CREATED, true);
        data.putExtra(MainActivity.TASK_NAME, taskName);
        data.putExtra(MainActivity.SELECTED_METHOD, selectedMethod);
        MainActivity.tasks.add(task);
        setResult(RESULT_OK, data);


        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss aaa z");
        DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM);

        Toast.makeText(this, simpleDateFormat.format(startTaskCalendar.getTime()), Toast.LENGTH_LONG).show();

        finish();
    }

    public void startDateButtonClicked(View view) {
        Intent intent = new Intent(this, CalendarActivity.class);
        intent.putExtra("type", "Выбор стартовой даты");
        isStartDateAdded = true;
        mStartForResult.launch(intent);

        //mStartForResult.launch(intent);
    }

    public void endDateButtonClicked(View view) {
        Intent intent = new Intent(this, CalendarActivity.class);
        intent.putExtra("type", "Выбор конечной даты");
        isEndDateAdded = true;
        mStartForResult.launch(intent);
    }

    public void addStartTimeClicked(View view) {
         addStartTime();
    }

    public void addEndTimeClicked(View view) {
        addEndTime();
    }

    public void setStartDate(String startDate) {
        startDateButton.setText(startDate);
    }

    public void setEndDate(String endDate) {
        endDateButton.setText(endDate);
    }

    public void addStartTime() {
        //final Calendar calendar = Calendar.getInstance();
        int hour = startTaskCalendar.get(Calendar.HOUR_OF_DAY);
        int minute = startTaskCalendar.get(Calendar.MINUTE);


        TimePickerDialog.OnTimeSetListener deadlineTimeListener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {
                if(timePicker.isShown()) {
                    startTaskCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                    startTaskCalendar.set(Calendar.MINUTE, minute);
                    String chosenTime = hourOfDay + ":" + minute;
                    addStartTimeButton.setText(chosenTime);
                }
            }
        };
        TimePickerDialog timePickerDialog = new TimePickerDialog(this, 3, deadlineTimeListener, hour, minute, true);

        timePickerDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", timePickerDialog);
        timePickerDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Закрыть", timePickerDialog);
        timePickerDialog.show();

    }

    public void addEndTime() {
        //final Calendar calendar = Calendar.getInstance();
        int hour = endTaskCalendar.get(Calendar.HOUR_OF_DAY);
        int minute = endTaskCalendar.get(Calendar.MINUTE);


        TimePickerDialog.OnTimeSetListener deadlineTimeListener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {
                if(timePicker.isShown()) {
                    endTaskCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                    endTaskCalendar.set(Calendar.MINUTE, minute);
                    String chosenTime = hourOfDay + ":" + minute;
                    addEndTimeButton.setText(chosenTime);
                }
            }
        };
        TimePickerDialog timePickerDialog = new TimePickerDialog(this, 3, deadlineTimeListener, hour, minute, true);

        timePickerDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", timePickerDialog);
        timePickerDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Закрыть", timePickerDialog);
        timePickerDialog.show();

    }


}