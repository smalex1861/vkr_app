package com.example.test;



import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import java.text.SimpleDateFormat;


public class CourseWorkActivity extends AppCompatActivity {

    public LinearLayout cardsLayout;
    public Button addFirstCard;
    public int taskIndex;
    public static long timeDifference;
    public static long timeInterval = 0;
    boolean isNotificationEnabled;

    String method;

    ActivityResultLauncher<Intent> mStartForResult = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {

                    if (result.getResultCode() == Activity.RESULT_OK) {
                        Intent intent = result.getData();
                        int cardIndex = intent.getIntExtra("cardIndex", 0);
                        addKanbanCard(cardIndex);


                    } else {

                    }
                }
            });

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_course_work);
        cardsLayout = findViewById(R.id.cardsLayout);
        addFirstCard = findViewById(R.id.addNewCardButton);
        //openData();

        Intent intent = getIntent();
        taskIndex = intent.getIntExtra("taskIndex", 0);
        cancelAlarm(taskIndex);
        String taskName = MainActivity.tasks.get(taskIndex).name;
        method = MainActivity.tasks.get(taskIndex).method;


        Switch courseWorkNotificationSwitch = findViewById(R.id.courseWorkNotificationSwitch);
        courseWorkNotificationSwitch.setChecked(false);



        courseWorkNotificationSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                isNotificationEnabled = isChecked;
                if(!isChecked){ cancelAlarm(taskIndex); }
            }
        });



        if(MainActivity.tasks.get(taskIndex).cards.size() != 0) {
            for(int idx = 0; idx < MainActivity.tasks.get(taskIndex).cards.size(); idx ++)
            {
               addKanbanCard(idx);
            }
        }
        timeDifference = MainActivity.tasks.get(taskIndex).startDate.getTime();
        TextView taskNameText = findViewById(R.id.header);
        taskNameText.setText(taskName);


    }

    @Override
    protected void onStop() {
        super.onStop();
        saveData();
        if(isNotificationEnabled) {
            startAlarm(taskIndex, timeDifference, timeInterval);
        }

    }

    @SuppressLint("UseCompatLoadingForDrawables")
    public void addKanbanCard(int cardIndex){

        String cardName = MainActivity.tasks.get(taskIndex).cards.get(cardIndex).name;
        String cardDescription = MainActivity.tasks.get(taskIndex).cards.get(cardIndex).description;

        if (addFirstCard.getParent() != null) {
            cardsLayout.removeView(addFirstCard);
        }
        addFirstCard.setVisibility(View.INVISIBLE);
        float factor = addFirstCard.getResources().getDisplayMetrics().density;

        LinearLayout kanbanCardLayout = new LinearLayout(this);
        kanbanCardLayout.setOrientation(LinearLayout.VERTICAL);
        kanbanCardLayout.setBackground(getDrawable(R.drawable.whitebuttonbackground));

        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        lp.setMargins((int) (20 * factor),(int) (10 * factor), (int) (20 * factor), 0);
        kanbanCardLayout.setLayoutParams(lp);


        TextView CardNameText = new TextView(this);
        CardNameText.setText(cardName);
        CardNameText.setGravity(Gravity.CENTER_HORIZONTAL);
        CardNameText.setTextSize(TypedValue.COMPLEX_UNIT_SP,16);
        CardNameText.setTextColor(Color.BLACK);
        CardNameText.setTypeface(Typeface.DEFAULT_BOLD);
        //CardNameText.setLayoutParams(lp);


        TextView CardDescription = new TextView(this);
        CardDescription.setText(cardDescription);
        CardDescription.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
        CardDescription.setTextColor(Color.BLACK);

        kanbanCardLayout.addView(CardNameText);
        kanbanCardLayout.requestLayout();
        kanbanCardLayout.addView(CardDescription);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        TextView deadlineCardInfo = new TextView(this);
        deadlineCardInfo.setText("Дедлайн: " + simpleDateFormat.format(MainActivity.tasks.get(taskIndex).cards.get(cardIndex).deadlineDate));
        kanbanCardLayout.addView(deadlineCardInfo);
        kanbanCardLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Context context = getApplicationContext();
                Intent intent = new Intent(context, KanbanCardActivity.class);
                intent.putExtra("cardName", cardName);
                intent.putExtra("cardDescription", cardDescription);
                intent.putExtra("taskIndex", taskIndex);
                intent.putExtra("cardIndex", cardIndex);
                startActivity(intent);
            }
        });

        LinearLayout.LayoutParams checkBoxLP = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        checkBoxLP.gravity = Gravity.RIGHT;

        CheckBox doneCheckBox = new CheckBox(this);
        doneCheckBox.setLayoutParams(checkBoxLP);

        doneCheckBox.setChecked(MainActivity.tasks.get(taskIndex).cards.get(cardIndex).isDone);

        doneCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
                MainActivity.tasks.get(taskIndex).cards.get(cardIndex).isDone = checked;
            }
        });

        kanbanCardLayout.addView(doneCheckBox);

        cardsLayout.addView(kanbanCardLayout);




        // kanbanCardButton.set


    }



    public void addNewCardButtonClicked(View view) {
        //addKanbanCard("Header", "Lorem ipsum dolor sit amet");
        Intent intent = new Intent(this, AddKanbanCardActivity.class);
        intent.putExtra("taskID", taskIndex);
        mStartForResult.launch(intent);
    }

    public void saveData() {
        boolean result = JSONHelper.exportToJSON(this, MainActivity.tasks);
    }

    public void settingsClicked(View view) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        String formattedStartDate = simpleDateFormat.format(MainActivity.tasks.get(taskIndex).startDate.getTime());
        String formattedEndDate = simpleDateFormat.format(MainActivity.tasks.get(taskIndex).endDate.getTime());
        Intent intent = new Intent(this, SettingsActivity.class);
        intent.putExtra("taskIndex", taskIndex);
        intent.putExtra("type", "courseWork");
        intent.putExtra("startDate",formattedStartDate);
        intent.putExtra("endDate", formattedEndDate);
        startActivity(intent);
    }

    public void startAlarm(int taskIndex, long time, long interval) {
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(this, AlertReceiver.class);
        intent.putExtra("taskIndex", taskIndex);
        intent.putExtra("taskName", MainActivity.tasks.get(taskIndex).name);
        intent.putExtra("subTaskName", "");
        intent.putExtra("activityType", "Курсовая работа");
        intent.setAction("actionstring" + System.currentTimeMillis());
        //Toast.makeText(this, "taskIndex " + taskIndex, Toast.LENGTH_LONG).show();
        PendingIntent pendingIntent = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
            pendingIntent = PendingIntent.getBroadcast(this, taskIndex, intent, PendingIntent.FLAG_MUTABLE);
        }
        else {
            pendingIntent = PendingIntent.getBroadcast(this, taskIndex, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        }
        if(interval > 0) {
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, time, interval, pendingIntent);
            //Toast.makeText(this, simpleDateFormat.format(timeDifference), Toast.LENGTH_LONG).show();
        }
        else {
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, time, pendingIntent);
        }
    }

    public void cancelAlarm(int taskIndex){
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(this, AlertReceiver.class);
        intent.putExtra("taskIndex", taskIndex);
        intent.putExtra("taskName", MainActivity.tasks.get(taskIndex).name);
        intent.putExtra("subTaskName", "");
        intent.putExtra("activityType", "Курсовая работа");


        PendingIntent pendingIntent = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
            pendingIntent = PendingIntent.getBroadcast(this, taskIndex, intent, PendingIntent.FLAG_MUTABLE);
        }
        else {
            pendingIntent = PendingIntent.getBroadcast(this, taskIndex, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        }

        alarmManager.cancel(pendingIntent);
    }

    public void homeButtonClicked(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    public void timeBlockLayoutClicked(View view) {
    }
}