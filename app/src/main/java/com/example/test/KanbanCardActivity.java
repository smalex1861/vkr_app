package com.example.test;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class KanbanCardActivity extends AppCompatActivity {

    public String cardName;
    public String cardDescription;
    Button addFirstSubtaskButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_kanban_card);
        Intent intent = getIntent();
        cardName = intent.getStringExtra("cardName");
        cardDescription = intent.getStringExtra("cardDescription");
        addFirstSubtaskButton = findViewById(R.id.addFirstSubtaskButton);

        TextView cardNameHeader = findViewById(R.id.chooseDateHeader);
        cardNameHeader.setText(cardName);

        //LinearLayout descriptionLayout = findViewById(R.id.descriptionLayout);
        TextView descriptionText = findViewById(R.id.descriptionText);
        descriptionText.setText(cardDescription);

    }

    public void addSubtaskClicked(View view) {
    }

    public void addSubtask() {
        LinearLayout layOut = findViewById(R.id.layOut);
        if(addFirstSubtaskButton.getParent() != null) {
            layOut.removeView(addFirstSubtaskButton);
        }

        addFirstSubtaskButton.setVisibility(View.INVISIBLE);
        float factor = addFirstSubtaskButton.getResources().getDisplayMetrics().density;


    }

}