package com.example.test;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class WelcomeActivity extends AppCompatActivity {
   //private TextView testText;
   // public SharedPreferences prefs;
    public String textValue;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        getSupportActionBar().hide();
        //Bundle arguments = getIntent().getExtras();
       // int mainTextViewId = (int) arguments.get("testTextViewId");
       // testText = findViewById(mainTextViewId);
       //testText = findViewById(R.id.testTextView);
        //testText.setText("123");
        //prefs = getSharedPreferences("firstRun", MODE_PRIVATE);

    }

    public void TestClick(View view) {
        //Intent intent = new Intent(this, .class);
       // startActivity(intent);
    }

    public void ApplyClick(View view) {
        //prefs.edit().putBoolean("firstRun", false);
        Intent data = new Intent();
        data.putExtra(MainActivity.RESPONSE_MESSAGE, "Apply");
        setResult(RESULT_OK, data);
       // intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP & Intent.FLAG_ACTIVITY_SINGLE_TOP);
        finish();
    }


    public void DeclineClick(View view) {
        Intent data = new Intent();
        data.putExtra(MainActivity.RESPONSE_MESSAGE, "Decline");
        setResult(RESULT_OK, data);
        finish();
    }
}